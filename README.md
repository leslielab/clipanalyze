# README #

This packaged is intended for analysis of CLIP-seq data, including HITS-CLIP, iCLIP, eCLIP.

*Downloads* section contains the most recent stable version of the package. It also contains a vignette that explains how to install and use the package.
